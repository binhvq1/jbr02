public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("cat");
        Animal animal2 = new Animal("dog");

        System.out.println(animal1.toString());
        System.out.println(animal2.toString());

        Mammal mammal1 = new Mammal("elephant");
        Mammal mammal2 = new Mammal("pig");

        System.out.println(mammal1.toString());
        System.out.println(mammal2.toString());

        Cat cat1 = new Cat("kitty");
        Cat cat2 = new Cat("hello");    

        System.out.println(cat1.toString());
        System.out.println(cat2.toString());

        cat1.greets();
        cat2.greets();

        Dog dog1 = new Dog("lulu");
        Dog dog2 = new Dog("lala");

        System.out.println(dog1.toString());
        System.out.println(dog2.toString());

        dog1.greets();
        dog2.greets();

        dog1.greets(dog2);


    }
}
