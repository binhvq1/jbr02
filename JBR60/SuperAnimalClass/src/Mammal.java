public class Mammal extends Animal {

    public Mammal() {
        
    }

    public Mammal(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return String.format("Mammal [Animal [Name = %S]]", name) ;
    }
}
