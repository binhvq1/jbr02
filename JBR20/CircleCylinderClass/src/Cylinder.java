public class Cylinder extends Circle {
    double height = 1.0;
    double volume;

    public Cylinder() {

    }

    public Cylinder(double radius) {
        this.radius = radius;
    }

    public Cylinder(double radius,String color) {
        this.radius = radius;
        this.color = color;
    }

    public Cylinder(double radius,String color,double height) {
        this.radius = radius;
        this.color = color;
        this.height = height;
    }

    public Cylinder(double radius,double height) {
        this.radius = radius;
        this.height = height;
    }

    public Cylinder(double radius,double height, String color) {
        this.radius = radius;
        this.height = height;
        this.color = color;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getVolume() {
        volume = Math.PI * Math.pow(radius, 2);
        return volume;
    }

    @Override
    public String toString() {
        return "Cylinder [height=" + height + ", radius=" + radius + ", color=" + color + "]";
    }

    
}
