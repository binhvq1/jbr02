public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1,"Lan",20);
        Customer customer2 = new Customer(2,"Hong",30);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        Account account1 = new Account(3, customer1,4500.0);
        Account account2 = new Account(4, customer2, 8500.0);

        System.out.println(account1.toString());
        System.out.println(account2.toString());

    }
}
