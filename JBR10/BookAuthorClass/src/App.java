import java.beans.BeanProperty;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Nam","namvo@gmail.com",'m');
        Author author2 = new Author("phuong","phuongvo@gmail.com",'f');

        System.out.println(author1.toString());
        System.out.println(author2.toString());

        Book book1 = new Book("Hong",author1,10000);
        Book book2 = new Book("Hung",author2,20000,2);

        System.out.println(book1.toString());
        System.out.println(book2.toString());

    }
}
