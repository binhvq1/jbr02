import java.util.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Nam",true,"vip");
        Customer customer2 = new Customer("Phuong",false,"member");

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        LocalDateTime localDateTime = LocalDateTime.now();
        Date dateday = (Date) Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        Visit visit1 = new Visit(customer1, dateday,10000,10000);
        Visit visit2 = new Visit(customer2, dateday,20000,20000);

        System.out.println(visit1.toString());
        System.out.println(visit2.toString());

        System.out.println(visit1.getTotalExpense());
        System.out.println(visit2.getTotalExpense());

        System.out.println("Tong cua 2 chuyen di");
        System.out.println(visit1.getTotalExpense() + visit2.getTotalExpense());
    }
}
