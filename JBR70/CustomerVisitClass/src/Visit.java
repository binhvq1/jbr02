import java.util.Date;

public class Visit {
    Customer customer;
    Date date;
    double serviceExpense;
    double productExpense;
    
    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }
    
    public Visit(Customer customer, Date date, double serviceExpense, double productExpense) {
        this.customer = customer;
        this.date = date;
        this.serviceExpense = serviceExpense;
        this.productExpense = productExpense;
    }

    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public double getServiceExpense() {
        return serviceExpense;
    }
    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }
    public double getProductExpense() {
        return productExpense;
    }
    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }
    
    public double getTotalExpense() {
        double totalExpense = productExpense + serviceExpense;
        return totalExpense;
    }
    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpense=" + productExpense
                + ", serviceExpense=" + serviceExpense + "]";
    }

    
}
