public class Customer {
    int customerID;
    String customerName;
    int customerDiscount;
    double amountAfterDiscount;

    public Customer() {
        super();
    }

    public Customer(int customerID,String customerName,int customerDiscount,double amountAfterDiscount) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.customerDiscount = customerDiscount;
        this.amountAfterDiscount = amountAfterDiscount;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getCustomerDiscount() {
        return customerDiscount;
    }

    public void setCustomerDiscount(int customerDiscount) {
        this.customerDiscount = customerDiscount;
    }

    public double getAmountAfterDiscount() {
        amountAfterDiscount = Invoice.amount * customerDiscount/100;
        return amountAfterDiscount;
    }

    public void setAmountAfterDiscount(double amountAfterDiscount) {
        this.amountAfterDiscount = amountAfterDiscount;
    }

    @Override
    public String toString() {
        return "Customer [amountAfterDiscount=" + amountAfterDiscount + ", customerDiscount=" + customerDiscount
                + ", customerID=" + customerID + ", customerName=" + customerName + "]";
    }

    
}
