public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(10,"Nam",10,10.0);
        Customer customer2 = new Customer(20,"Phuong",20,20.0);

        System.out.println(customer1);
        System.out.println(customer2);

        Invoice invoice1 = new Invoice(1, customer1, 1000);
        Invoice invoice2 = new Invoice(2, customer2, 2000);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());

        System.out.println((invoice1.getAmount()*(invoice1.getCustomer().getCustomerDiscount()))/ 100);
        System.out.println((invoice2.getAmount()*(invoice2.getCustomer().getCustomerDiscount()))/ 100);

    }
}
