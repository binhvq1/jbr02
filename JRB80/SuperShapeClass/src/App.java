public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("yellow");
        Shape shape3 = new Shape("green",false);

        System.out.println(shape1);
        System.out.println(shape2);
        System.out.println(shape3);

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle("green", true, 3.0);

        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);

        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(1.5);
        Rectangle rectangle3 = new Rectangle("green",true, 2.0, 2.0);

        System.out.println(rectangle1);
        System.out.println(rectangle2);
        System.out.println(rectangle3);

        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square("green", true, 2.0);

        System.out.println(square1);
        System.out.println(square2);
        System.out.println(square3);

        System.out.println("Dien tich hinh tron 1");
        System.out.println(circle1.getArea());
        System.out.println("Dien tich hinh tron 2");
        System.out.println(circle2.getArea());
        System.out.println("Dien tich hinh tron 3");
        System.out.println(circle3.getArea());

        System.out.println("Chu vi hinh tron 1");
        System.out.println(circle1.getPerimeter());
        System.out.println("Chu vi hinh tron 2");
        System.out.println(circle2.getPerimeter());
        System.out.println("Chu vi hinh tron 3");
        System.out.println(circle3.getPerimeter());

        System.out.println("Dien tich hinh chu nhat 1");
        System.out.println(rectangle1.getArea());
        System.out.println("Dien tich hinh chu nhat 2");
        System.out.println(rectangle2.getArea());
        System.out.println("Dien tich hinh chu nhat 3");
        System.out.println(rectangle3.getArea());

        System.out.println("Chu vi hinh chu nhat 1");
        System.out.println(rectangle1.getPerimeter());
        System.out.println("Chu vi hinh chu nhat 2");
        System.out.println(rectangle2.getPerimeter());
        System.out.println("Chu vi hinh chu nhat 3");
        System.out.println(rectangle3.getPerimeter());

        System.out.println("Dien tich hinh vuong 1");
        System.out.println(square1.getArea());
        System.out.println("Dien tich hinh vuong 2");
        System.out.println(square2.getArea());
        System.out.println("Dien tich hinh vuong 3");
        System.out.println(square3.getArea());

        System.out.println("Chu vi hinh vuong 1");
        System.out.println(square1.getPerimeter());
        System.out.println("Chu vi hinh vuong 2");
        System.out.println(square2.getPerimeter());
        System.out.println("Chu vi hinh vuong 3");
        System.out.println(square3.getPerimeter());

    }
}
