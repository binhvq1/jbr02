public class Square extends Rectangle {
    double side = width;
    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(String color, boolean filled, double side) {
        this.color = color;
        this.filled = filled;
        this.side = side;
    }

    public double getArea() {
        double getArea = side * side;
        return getArea;
    }

    public double getPerimeter() {
        double getPerimeter = side * 4;
        return getPerimeter;
    }

    @Override
    public String toString() {
        return String.format("Square [Rectangle[Shape [color = %s,filled = %s], width = %s]]", color, filled, side);
    }
}
