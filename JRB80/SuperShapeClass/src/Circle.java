public class Circle extends Shape {
    double radius = 1.0;

    public Circle() {
        super();
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        double getArea = Math.PI * Math.pow(radius, 2);
        return getArea;
    }

    public double getPerimeter() {
        double getPerimeter = Math.PI * radius * 2;
        return getPerimeter;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

}
