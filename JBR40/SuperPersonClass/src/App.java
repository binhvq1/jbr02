public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Nam","Binh Duong");
        Person person2 = new Person("Phuong","Ho Chi Minh");

        System.out.println(person1.toString());
        System.out.println(person2.toString());

        Student student1 = new Student("Hung", "Can Tho", "Ky Su", 2020, 1.0);
        Student student2 = new Student("Hoang", "Rach Gia", "Cu NHan", 2021, 2.0);
        System.out.println(student1.toString());
        System.out.println(student2.toString());

        Staff staff1 = new Staff("Minh", "Binh Thuan", "Hong Bang", 10000);
        Staff staff2 = new Staff("Man", "Dong Thap", "Van Lang", 20000);

        System.out.println(staff1.toString());
        System.out.println(staff2.toString());
    }
}
